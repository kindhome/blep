package io.kindhome.blep

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothProfile.STATE_CONNECTED
import android.bluetooth.BluetoothProfile.STATE_CONNECTING
import android.bluetooth.BluetoothProfile.STATE_DISCONNECTED
import android.bluetooth.BluetoothProfile.STATE_DISCONNECTING
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.ParcelUuid
import android.util.Log
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import no.nordicsemi.android.ble.BleManager
import no.nordicsemi.android.ble.ReadRequest
import no.nordicsemi.android.ble.WriteRequest
import no.nordicsemi.android.ble.annotation.LogPriority
import no.nordicsemi.android.ble.data.Data
import no.nordicsemi.android.ble.observer.ConnectionObserver
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat
import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanFilter
import no.nordicsemi.android.support.v18.scanner.ScanResult
import no.nordicsemi.android.support.v18.scanner.ScanSettings
import no.nordicsemi.android.support.v18.scanner.ScanSettings.MATCH_MODE_AGGRESSIVE
import no.nordicsemi.android.support.v18.scanner.ScanSettings.SCAN_MODE_LOW_LATENCY
import java.util.UUID


class BlepManager(context: Context): BleManager(context) {
  private lateinit var gatt: BluetoothGatt

  override fun log(@LogPriority priority: Int, message: String) {
    Log.println(priority, "BLEP", message)
  }
  override fun initialize() {
    log(Log.INFO, "Initializing");
    // Initialize your device.
    // This means e.g. enabling notifications, setting notification callbacks, or writing
    // something to a Control Point characteristic.
    // Kotlin projects should not use suspend methods here, as this method does not suspend.
    requestMtu(517)
            .enqueue();
  }

  override fun isRequiredServiceSupported(gatt: BluetoothGatt): Boolean {
    this.gatt = gatt
    return true
  }

  fun getCharacteristic(serviceId: String, characteristicId: String): BluetoothGattCharacteristic? {
    return gatt.getService(UUID.fromString(serviceId))?.run {
      getCharacteristic(UUID.fromString(characteristicId))
    }
  }

  public override fun writeCharacteristic(
    characteristic: BluetoothGattCharacteristic?,
    data: ByteArray?,
    writeType: Int
  ): WriteRequest {
    return super.writeCharacteristic(characteristic, data, writeType)
  }

  public override fun readCharacteristic(characteristic: BluetoothGattCharacteristic?): ReadRequest {
    return super.readCharacteristic(characteristic)
  }

  fun subscribeCharacteristic(characteristic: BluetoothGattCharacteristic?, eventSink: EventChannel.EventSink) {
    setNotificationCallback(characteristic).with { _: BluetoothDevice, data: Data ->
      eventSink.success(data.value)
    }
    enableNotifications(characteristic).done {
    }.enqueue()
  }
}

/** Blep */
class Blep: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  private lateinit var messenger: BinaryMessenger
  private lateinit var dartMessenger: BinaryMessenger

  private lateinit var context: Context

  private var managers = mutableMapOf<String, BlepManager>()

  private var scanner = BluetoothLeScannerCompat.getScanner()

  var devices = mutableMapOf<String, BluetoothDevice>()

  fun log(message: String) {
    Log.d("BLEP", message)
  }

  inner class ScanCallbackBlep(private var eventSink: EventChannel.EventSink, private var filters: List<ScanFilter>): ScanCallback() {
    val found = mutableSetOf<BluetoothDevice>()

    fun addResult(result: ScanResult) {
      if (result.device.name == null) {
        return
      }
      for (filter in filters) {
        if (!filter.matches(result)) {
          return
        }
      }
      if (!found.contains(result.device)) {
        found.add(result.device)
        eventSink.success(hashMapOf("name" to result.device.name, "id" to result.device.address, "manufacturerId" to result.scanRecord?.manufacturerSpecificData?.valueAt(0)))
      }
      devices[result.device.address] = result.device
    }

    @Override
    override fun onScanResult(callbackType: Int, result: ScanResult) {
      addResult(result)
    }

    @Override
    override fun onBatchScanResults(results: MutableList<ScanResult>) {
      for (result in results) {
        addResult(result)
      }
    }
  }

  inner class ScanStreamHandler(private var filters: List<ScanFilter>, private var settings: ScanSettings) : EventChannel.StreamHandler {
    private lateinit var scannerCallback: ScanCallbackBlep

    override fun onListen(listener: Any?, eventSink: EventChannel.EventSink) {
      scannerCallback = ScanCallbackBlep(eventSink, filters)
      scanner.startScan(filters, settings, scannerCallback)
    }

    override fun onCancel(listener: Any?) {
      scanner.stopScan(scannerCallback)
    }
  }
  override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    dartMessenger = flutterPluginBinding.flutterEngine.dartExecutor.binaryMessenger
    messenger = flutterPluginBinding.binaryMessenger
    channel = MethodChannel(messenger, "blep")
    channel.setMethodCallHandler(this)

    context = flutterPluginBinding.applicationContext
  }

  override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
    when (call.method) {
      "scanDevices" -> {
        val events = EventChannel(dartMessenger, "scan_blep")
        val name: String? = call.argument<String>("name")
        val withServices: List<String> = call.argument<List<String>>("withServices")!!
        val manufacturerId: Int? = call.argument<Int>("manufacturerId")
        val manufacturerData: ByteArray? = call.argument<ByteArray>("manufacturerData")
        val filters: MutableList<ScanFilter> = withServices.map { ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(it)).build() }.toMutableList()
        if (name != null) {
          filters.add(ScanFilter.Builder().setDeviceName(name).build())
        }
        if (manufacturerId != null) {
          filters.add(ScanFilter.Builder().setManufacturerData(manufacturerId, manufacturerData).build())
        }
        val settings: ScanSettings = ScanSettings.Builder()
                .setMatchMode(MATCH_MODE_AGGRESSIVE)
                .setScanMode(SCAN_MODE_LOW_LATENCY)
                .build()
        events.setStreamHandler(ScanStreamHandler(filters, settings))
        result.success(null)
      }
      "connectToDevice" -> {
        val id: String = call.argument<String>("id")!!
        val phy: Int? = call.argument<Int>("phy")
        if (devices.containsKey(id)) {
          connectToDevice(devices[id]!!, phy, result)
          return
        }
        val filters = listOf(ScanFilter.Builder().setDeviceAddress(id).build())
        val callback = object : ScanCallback() {
          override fun onScanResult(callbackType: Int, scanResult: ScanResult) {
            val device = scanResult.device
            if (!devices.containsKey(device.address)) {
              devices[device.address] = device
              connectToDevice(device, phy, result)
            }
          }

          override fun onScanFailed(errorCode: Int) {
            log("The device was not found")
            result.error("The device was not found", "The device was not found", null)
          }
        }
        scanner.startScan(filters, null, callback)
        Handler(Looper.getMainLooper()).postDelayed({
          scanner.stopScan(callback)
        }, 5_000)
      }
      "writeData" -> {
        val id: String = call.argument<String>("id")!!
        val serviceId: String = call.argument<String>("service")!!
        val characteristicId: String = call.argument<String>("characteristic")!!
        val data: ByteArray = call.argument<ByteArray>("data")!!
        val manager: BlepManager
        if (managers.containsKey(id)) {
          manager = managers[id]!!
        } else {
          log("Write: device not connected")
          return
        }

        // TODO(@MM) Investigate fail and invalid callbacks in BleManager.
        val characteristic = manager.getCharacteristic(serviceId, characteristicId)
        manager.writeCharacteristic(characteristic, data, BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT).done {
          result.success(null)
        }.enqueue()
      }
      "readData" -> {
        val id: String = call.argument<String>("id")!!
        val serviceId: String = call.argument<String>("service")!!
        val characteristicId: String = call.argument<String>("characteristic")!!
        val manager: BlepManager
        if (managers.containsKey(id)) {
          manager = managers[id]!!
        } else {
          log("Read: device not connected")
          result.error("Read: device not connected", "Read: device not connected", null)
          return
        }

        val characteristic = manager.getCharacteristic(serviceId, characteristicId)
        if (characteristic == null) {
          result.error("Read: characteristic not found", "Read: characteristic not found", null)
          return
        }
        manager.readCharacteristic(characteristic).with { _, data ->
          result.success(data.value)
        }.fail { _, _ -> result.success(byteArrayOf()) }.enqueue()
      }
      "subscribeCharacteristic" -> {
        val id: String = call.argument<String>("id")!!
        val serviceId: String = call.argument<String>("service")!!
        val characteristicId: String = call.argument<String>("characteristic")!!
        val manager: BlepManager
        if (managers.containsKey(id)) {
          manager = managers[id]!!
        } else {
          result.error("Subscribe: device not connected", "Subscribe: device not connected", null)
          return
        }
        val characteristic = manager.getCharacteristic(serviceId, characteristicId)
        if (characteristic == null) {
          result.error("Subscribe: characteristic not found", "Subscribe: characteristic not found", null)
          return
        }
        val channelName = characteristic.uuid.toString() + "@" + characteristic.service?.uuid.toString() + "@" + id
        result.success(channelName)
        val events = EventChannel(dartMessenger, channelName)
        events.setStreamHandler(object : EventChannel.StreamHandler {
          override fun onListen(listener: Any?, eventSink: EventChannel.EventSink) {
            manager.subscribeCharacteristic(characteristic, eventSink)
          }

          override fun onCancel(listener: Any?) {
            log("Subscription canceled.")
          }
        })

      }
      "getConnectionState" -> {
        val id: String = call.argument<String>("id")!!
        result.success(if (managers.containsKey(id)) managers[id]!!.connectionState else STATE_DISCONNECTED)
      }
      else -> {
        result.notImplemented()
      }
    }
  }

  private fun connectToDevice(device: BluetoothDevice, phy: Int?, result: MethodChannel.Result) {
    val manager = BlepManager(context)
    EventChannel(messenger, device.address).setStreamHandler(object: EventChannel.StreamHandler {
      override fun onListen(arguments: Any?, events: EventChannel.EventSink) {
        events.success(manager.connectionState)

        manager.connectionObserver = object: ConnectionObserver {
          override fun onDeviceConnecting(device: BluetoothDevice) {
            events.success(STATE_CONNECTING)
          }

          override fun onDeviceConnected(device: BluetoothDevice) {
            events.success(STATE_CONNECTING)
          }

          override fun onDeviceFailedToConnect(device: BluetoothDevice, reason: Int) {
            events.success(STATE_DISCONNECTED)
          }

          override fun onDeviceReady(device: BluetoothDevice) {
            events.success(STATE_CONNECTED)
          }

          override fun onDeviceDisconnecting(device: BluetoothDevice) {
            events.success(STATE_DISCONNECTING)
          }

          override fun onDeviceDisconnected(device: BluetoothDevice, reason: Int) {
            events.success(STATE_DISCONNECTED)
          }
        }
      }

      override fun onCancel(arguments: Any?) {
        manager.disconnect().enqueue()
      }
    })
    var connectionRequest = manager.connect(device).retry(3, 100).timeout(10_000).useAutoConnect(true)
    if (phy != null ) {
      connectionRequest = connectionRequest.usePreferredPhy(phy)
    }
    connectionRequest.done {
      managers[device.address] = manager
      result.success(null)
    }.enqueue()
  }

  override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
