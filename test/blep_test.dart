import 'package:flutter_test/flutter_test.dart';
import 'package:blep/blep.dart';
import 'package:blep/blep_platform_interface.dart';
import 'package:blep/blep_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockBlepPlatform
    with MockPlatformInterfaceMixin
    implements BlepPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final BlepPlatform initialPlatform = BlepPlatform.instance;

  test('$MethodChannelBlep is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelBlep>());
  });

  test('getPlatformVersion', () async {
    Blep blep = Blep();
    MockBlepPlatform fakePlatform = MockBlepPlatform();
    BlepPlatform.instance = fakePlatform;

    expect(await blep.getPlatformVersion(), '42');
  });
}
