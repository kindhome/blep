import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:blep/blep.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _blep = Blep();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // TODO(@MM) Request Bluetooth on.

    await Permission.bluetooth.request();
    await Permission.bluetoothScan.request();
    await Permission.bluetoothConnect.request();

    var device = await _blep.scanDevices().where((d) => d.name == "Markiza Testowa Marcin").first;

    device.connect().listen((event) {
      print(event.toString());
    });

    await _blep.writeData(device.id, "75c276c3-8f97-20bc-a143-b354244886d4", "6acf4f08-cc9d-d495-6b41-aa7e60c4e8a6", Uint8List.fromList([0x02]));
    var read = await _blep.readData(device.id, "00001800-0000-1000-8000-00805f9b34fb",   "00002a00-0000-1000-8000-00805f9b34fb");
    print(utf8.decode(read));
    _blep.subscribeCharacteristic(device.id, "75c276c3-8f97-20bc-a143-b354244886d4",   "D3D46A35-4394-E9AA-5A43-E7921120AAED").listen((event) {
      print(event);
    });
    await _blep.writeData(device.id, "75c276c3-8f97-20bc-a143-b354244886d4", "6acf4f08-cc9d-d495-6b41-aa7e60c4e8a6", Uint8List.fromList([0x01]));
    await _blep.writeData(device.id, "75c276c3-8f97-20bc-a143-b354244886d4", "6acf4f08-cc9d-d495-6b41-aa7e60c4e8a6", Uint8List.fromList([0x02]));
    await _blep.writeData(device.id, "75c276c3-8f97-20bc-a143-b354244886d4", "6acf4f08-cc9d-d495-6b41-aa7e60c4e8a6", Uint8List.fromList([0x00]));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: const Center(
          child: Text('Running BLEP\n'),
        ),
      ),
    );
  }
}
