## 0.1.0

* Initial release.

## 0.2.0

* Moved `DeviceConnectionState` to the main library.
* Removed `getConnectionState` method. All interested parties should listen for updates from `Stream` returned by `connectToAddress`.

## 0.2.1

* Fixed device scanning on iOS.

## 0.2.2

* Fixed packaging data. Incorrect repository link is fixed.

## 0.3.0
* Added a proper timeout for scanner
* * Added filtering by manufacturer data in scanner.
* Fixed building issues in example package.
* Fixed scanner filtering. Now instead of giant alternative a conjunction of filters will be used.

## 0.3.1
* Fixed connecting issues on Android

## 0.3.2

* Fixed invalid double write on iOS

## 0.4.0

* Added `manufacturerId` to `BluetoothDevice`.
* Added filtering only by manufacturer id in scanner (without data)

## 0.4.1

* Request bigger MTU on Android

## 0.4.2

*  Fix subscription when 2 or more devices are connected 
 
## 0.4.3

*  Fix `readData` on ios
 
