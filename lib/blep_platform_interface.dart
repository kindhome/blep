import 'dart:async';
import 'dart:typed_data';

import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'package:blep/blep.dart';
import 'package:blep/blep_method_channel.dart';

abstract class BlepPlatform extends PlatformInterface {
  /// Constructs a BlepPlatform.
  BlepPlatform() : super(token: _token);

  static final Object _token = Object();

  static BlepPlatform _instance = MethodChannelBlep();

  /// The default instance of [BlepPlatform] to use.
  ///
  /// Defaults to [MethodChannelBlep].
  static BlepPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [BlepPlatform] when
  /// they register themselves.
  static set instance(BlepPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Stream<BluetoothDevice> scanDevices(String? name, List<String> withServices,
      ManufacturerSpecificData? manufacturerSpecificData) async* {
    throw UnimplementedError('scanDevices() has not been implemented.');
  }

  Stream<DeviceConnectionState> connectToAddress(
      String address, int? phy) async* {
    throw UnimplementedError('connectToDevice() has not been implemented.');
  }

  Future<void> disconnectDevice(BluetoothDevice device) async {
    throw UnimplementedError('disconnectDevice() has not been implemented.');
  }

  Future<List<String>> discoverServices(BluetoothDevice device) async {
    return BlepPlatform.instance.discoverServices(device);
  }

  Future<void> writeData(String address, String service, String characteristic,
      Uint8List data) async {
    throw UnimplementedError('writeData() has not been implemented.');
  }

  Future<Uint8List> readData(
      String address, String service, String characteristic) async {
    throw UnimplementedError('readData() has not been implemented.');
  }

  Stream<Uint8List> subscribeCharacteristic(
      String address, String service, String characteristic) async* {
    throw UnimplementedError(
        'subscribeCharacteristic() has not been implemented.');
  }
}
