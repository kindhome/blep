import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'package:flutter_reactive_ble/flutter_reactive_ble.dart' as rble;

import 'package:blep/blep_platform_interface.dart';

class Connection {}

class ManufacturerSpecificData {
  ManufacturerSpecificData(this.id, this.data);

  int id;
  Uint8List? data;
}

class Blep {
  final _flutterReactiveBle = rble.FlutterReactiveBle();

  Stream<BluetoothDevice> scanDevices(
      {String? name,
      Duration? timeout,
      List<String> withServices = const [],
      ManufacturerSpecificData? manufacturerSpecificData}) async* {
    Stream<BluetoothDevice> result;
    if (Platform.isIOS) {
      var devices = _flutterReactiveBle.scanForDevices(
          withServices: withServices.map((e) => rble.Uuid.parse(e)).toList());
      if (name != null) {
        devices = devices.where((device) => device.name == name);
      }
      if (manufacturerSpecificData?.data == null) {
        devices = devices.where((device) => device.manufacturerData.length > 3);
      }
      if (manufacturerSpecificData != null) {
        if (manufacturerSpecificData.data != null) {
          devices = devices.where((device) => listEquals(
              device.manufacturerData,
              intToUint8List(manufacturerSpecificData.id) +
                  manufacturerSpecificData.data!));
        } else {
          devices = devices.where((device) => listEquals(
              device.manufacturerData.sublist(0, 2),
              intToUint8List(manufacturerSpecificData.id)));
        }
      }
      result = devices
          .map((device) => BluetoothDevice(name: device.name, id: device.id, manufacturerId: device.manufacturerData.sublist(2)));
    } else {
      result = BlepPlatform.instance
          .scanDevices(name, withServices, manufacturerSpecificData);
    }
    if (timeout != null) {
      result = result.timeout(timeout, onTimeout: (es) => es.close());
    }
    yield* result;
  }

  Stream<DeviceConnectionState> connectToAddress(String address,
      {Map<String, List<String>>? servicesWithCharacteristicsToDiscover,
      Duration? connectionTimeout,
      int? phy}) async* {
    if (Platform.isIOS) {
      var converted = servicesWithCharacteristicsToDiscover?.map((key, value) =>
          MapEntry(rble.Uuid.parse(key),
              value.map((e) => rble.Uuid.parse(e)).toList()));
      yield* _flutterReactiveBle
          .connectToDevice(
              id: address,
              servicesWithCharacteristicsToDiscover: converted,
              connectionTimeout: connectionTimeout)
          .map((state) => switch (state.connectionState) {
                rble.DeviceConnectionState.connected =>
                  DeviceConnectionState.connected,
                rble.DeviceConnectionState.connecting =>
                  DeviceConnectionState.connecting,
                rble.DeviceConnectionState.disconnected =>
                  DeviceConnectionState.disconnected,
                rble.DeviceConnectionState.disconnecting =>
                  DeviceConnectionState.disconnecting,
              });
    } else {
      yield* BlepPlatform.instance.connectToAddress(address, phy);
    }
  }

  Future<void> disconnectDevice(BluetoothDevice device) async {
    return BlepPlatform.instance.disconnectDevice(device);
  }

  Future<List<String>> discoverServices(BluetoothDevice device) async {
    return BlepPlatform.instance.discoverServices(device);
  }

  Future<void> writeData(String address, String service, String characteristic,
      List<int> data) async {
    if (Platform.isIOS) {
      var qCharacteristic = rble.QualifiedCharacteristic(
          serviceId: rble.Uuid.parse(service),
          characteristicId: rble.Uuid.parse(characteristic),
          deviceId: address);
      return await _flutterReactiveBle
          .writeCharacteristicWithoutResponse(qCharacteristic, value: data);
    } else {
      return BlepPlatform.instance.writeData(
          address, service, characteristic, Uint8List.fromList(data));
    }
  }

  Future<Uint8List> readData(
      String address, String service, String characteristic) async {
    if (Platform.isIOS) {
      var qCharacteristic = rble.QualifiedCharacteristic(
          serviceId: rble.Uuid.parse(service),
          characteristicId: rble.Uuid.parse(characteristic),
          deviceId: address);
      return await _flutterReactiveBle
          .readCharacteristic(qCharacteristic).then((value) => Uint8List.fromList(value));
    } else {
      return BlepPlatform.instance.readData(address, service, characteristic);
    }
  }

  // TODO(@MM) Uint8List vs List<int>
  Stream<List<int>> subscribeCharacteristic(
      String address, String service, String characteristic) async* {
    if (Platform.isIOS) {
      var qCharacteristic = rble.QualifiedCharacteristic(
          characteristicId: rble.Uuid.parse(characteristic),
          serviceId: rble.Uuid.parse(service),
          deviceId: address);
      yield* _flutterReactiveBle.subscribeToCharacteristic(qCharacteristic);
    } else {
      try {
        yield* BlepPlatform.instance
            .subscribeCharacteristic(address, service, characteristic);
      } on PlatformException {
        throw Exception("Characteristic $characteristic does not exist");
      }
    }
  }

  static Uint8List intToUint8List(int value){
    ByteData byteData = ByteData(4);
    byteData.setInt32(0, value, Endian.little);
    return byteData.buffer.asUint8List().sublist(0, 2);
  }
}

enum DeviceConnectionState {
  disconnected,
  connecting,
  connected,
  disconnecting,
}

class BluetoothDevice {
  BluetoothDevice({
    required this.name,
    required this.id,
    this.manufacturerId,
  });

  final String name;
  final String id;
  final Uint8List? manufacturerId;
  bool isConnected = false;

  Stream<DeviceConnectionState> connect({int? phy}) async* {
    // TODO(@MM) Resolve id vs address debate.
    yield* BlepPlatform.instance.connectToAddress(id, phy);
  }

  @override
  int get hashCode => id.hashCode;

  @override
  bool operator ==(Object other) {
    if (other is! BluetoothDevice) {
      return false;
    }
    // TODO: implement ==
    return name == other.name &&
        id == other.id &&
        isConnected == other.isConnected;
  }
}
