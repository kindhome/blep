import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'package:blep/blep.dart';
import 'package:blep/blep_platform_interface.dart';

/// An implementation of [BlepPlatform] that uses method channels.
class MethodChannelBlep extends BlepPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('blep');

  @override
  Stream<BluetoothDevice> scanDevices(String? name, List<String> withServices,
      ManufacturerSpecificData? manufacturerSpecificData) async* {
    const events = EventChannel("scan_blep");

    var arguments = {'name': name, 'withServices': withServices};

    if (manufacturerSpecificData != null) {
      arguments.addAll({
        'manufacturerId': manufacturerSpecificData.id,
        'manufacturerData': manufacturerSpecificData.data,
      });
    }

    await methodChannel.invokeMethod('scanDevices', arguments);

    yield* events.receiveBroadcastStream().map((deviceData) =>
        BluetoothDevice(name: deviceData['name'], id: deviceData['id'], manufacturerId: deviceData['manufacturerId']));
  }

  @override
  Stream<DeviceConnectionState> connectToAddress(
      String address, int? phy) async* {
    await methodChannel
        .invokeMethod('connectToDevice', {'id': address, 'phy': phy});
    yield* EventChannel(address)
        .receiveBroadcastStream()
        .map((event) => DeviceConnectionState.values[event]);
  }

  @override
  Future<void> disconnectDevice(BluetoothDevice device) async {
    await methodChannel.invokeMethod('disconnectDevice', {
      'id': device.id,
    });
  }

  // @override
  // Future<List<String>> discoverServices(BluetoothDevice device) async {
  //   await methodChannel.invokeMethod('discoverServices', {
  //     'id': device.id,
  //   });
  // }

  @override
  Future<void> writeData(String address, String service, String characteristic,
      Uint8List data) async {
    await methodChannel.invokeMethod('writeData', {
      'id': address,
      'service': service,
      'characteristic': characteristic,
      'data': data,
    });
  }

  @override
  Future<Uint8List> readData(
      String address, String service, String characteristic) async {
    return await methodChannel.invokeMethod('readData', {
      'id': address,
      'service': service,
      'characteristic': characteristic,
    });
  }

  @override
  Stream<Uint8List> subscribeCharacteristic(
      String address, String service, String characteristic) async* {
    final channelName =
        await methodChannel.invokeMethod('subscribeCharacteristic', {
      'id': address,
      'service': service,
      'characteristic': characteristic,
    });
    yield* EventChannel(channelName)
        .receiveBroadcastStream()
        .map((event) => event as Uint8List);
  }
}
